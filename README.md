# BaseHangul在线编解码器 basehangul-online

[体验本工具](https://jiew-kuejwiaeng.gitee.io/basehangul-online/)

[Try out the tool!](https://ayaka14732.github.io/basehangul-online/)

## 什么是谚文？ What is Hangul?

谚文是朝鲜民族所使用的表音文字系统，于1443年由朝鲜国王李祹创制，是朝鲜民族语言现行的正式文字。谚文由『字母』组合而成，一个谚文表示一个音节，进而推广为丰富的词汇。

Hangul is the Korean writing system that consists of syllabic characters representing the sounds of the Korean language. It was created in the 15th century and is now the official script of Korea. Hangul is composed of individual characters called "jamo" that represent syllables and can be combined to form complex words.

## 什么是BaseHangul？ What is BaseHangul?

与Base64类似，BaseHangul也是一种二进制编码，特点是使用谚文表示二进制数据。由于大韩民国的KS C 5601标准是以10位为基准编制的，而当今主流的二进制计算机处理的基础是8位，『8』与『10』的最小公倍数是『40』，故而BaseHangul是以『每40位（5字节）的数据转化为4个谚文』运作的。大韩民国的KS C 5601字符编码标准收录有谚文2350个，BaseHangul以字典顺序选取了其中1028个使用。谚文『흐』超出该标准范围是为了用作填充字符。

Similar to Base64, BaseHangul is a binary encoding that uses Hangul (Korean writing system) characters to represent binary data. It converts 5 bytes (40 bits) into 4 Hangul characters based on the KS C 5601 standard. The 40-bit conversion is chosen because it is the least common multiple of 8 and 10, which are the number of bits used to represent 1 character. 1028 Hangul characters out of 2350 in the standard are selected in dictionary order for the encoding. The character "흐" is outside of the selected range but used as a padding character.

## 什么是**BaseHangul在线编解码器**？ What is the  **Online BaseHangul Encoder and Decoder** ?

**BaseHangul在线编解码器**是一款䏻快速高效地对BaseHangul数据编解码的先进工具。该工具基于JavaScript实现，䏻在您的浏览器中提供顺畅的用户体验。处理是在本地完成的，不会将任何数据传递过与第三方，您的信息将始终是隐秘而安全的。只需点一下，您就可以轻松地将文本转换为谚文字符组群，反之亦然。无论您是在BaseHangul或相关领域的专业人士，还是仅为了寻求方便编解码BaseHangul数据途径的爱好者，这个在线工具都是完美的解决方案。

Online BaseHangul Encoder and Decoder is a cutting-edge tool designed for quick and efficient encoding and decoding of BaseHangul data. With its fast and reliable implementation in JavaScript, this tool provides a seamless user experience right in your web browser. The computation is done locally, ensuring that your information is kept secure and confidential at all times, as it does not transmit any data to any third-party servers. With just one click, you can easily convert text data into Hangul characters and vice versa, saving you time and effort. Whether you're a professional or simply looking for a convenient way to encode and decode BaseHangul data, this online tool is the perfect solution.

## 更多信息 More information

**BaseHangul在线编解码器**基于[BaseHangul 1.1编码标准](https://basehangul.github.io/)的JavaScript项目——[basehangul-javascript](https://github.com/basehangul/basehangul-javascript)。该项目提供了一个可靠而高效的编解码二进制数据为谚文的平台。

The Online BaseHangul Encoder and Decoder is built upon the [basehangul-javascript](https://github.com/basehangul/basehangul-javascript) project, which is a JavaScript implementation of the BaseHangul 1.1 encoding standard. The project provides a reliable and efficient platform for encoding and decoding binary data into Hangul characters.

有关BaseHangul的更多内容（发展历史、其它实例项目等），请访问[官方网站](https://basehangul.github.io/)。此外，该网站还提供了大量关于如何使用BaseHangul的要点，包括教程、示例和API。无论您是开发人员、研究人员还是只是想了解更多关于BaseHangul资讯的爱好者，官方网站都是一个极好的平台，提供了您初步了解所需的所有信息。

For more information on BaseHangul, we encourage you to visit the [official website](https://basehangul.github.io/). Here, you will find detailed information about the encoding standard, including its history, development, and implementation. Additionally, the site provides a wealth of resources and information about how to use BaseHangul, including tutorials, examples, and APIs. Whether you're a developer, researcher, or simply interested in learning more about BaseHangul, the official website is an excellent resource that provides all the information you need to get started.
